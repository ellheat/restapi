/**
 * Created by patry on 22.01.2016.
 */

const Mysql = require('mysql');
const Config = require('../../config/config.js');

const database = Mysql.createConnection(Config.dbConnection);
const Team = {};
const Response = [
    {
        result: ""
    }
];


//url: /team

Team.getAllTeams = function(fn) {
    database.query('SELECT * FROM team', function(err, rows) {
        if (!err) {
            return fn(rows);
        } else {
            return fn(err);
        }
    });
};

Team.addTeam = function(payload, fn) {
    database.query('INSERT INTO team (name, city, state) VALUES("'+payload.name+'","'+payload.city+'","'+payload.state+'")', function(err) {
        if (!err) {
            Response[0].result = 'team has been added';
            return fn(Response);
        } else {
            return fn(err);
        }
    });
};


//url: /team/{id}

Team.getSingleTeam = function(id, fn) {
    database.query('SELECT * FROM team WHERE id='+id , function(err, rows) {
        if (rows != "") {
            return fn(rows);
        } else {
            return fn(err);
        }
    });
};

Team.updateSingleTeam = function(id, payload, fn) {
    database.query('UPDATE team SET name="'+payload.name+'", city="'+payload.city+'", state="'+payload.state+'" WHERE id='+id , function(err, res) {
        if (res.affectedRows > 0) {
            Response[0].result = 'team has been updated';
            return fn(Response);
        } else {
            return fn(err);
        }
    });
};

Team.deleteSingleTeam = function(id, fn) {
    database.query('DELETE FROM team WHERE id='+id , function(err, res) {
        if (res.affectedRows > 0) {
            Response[0].result = 'team has been deleted';
            return fn(Response);
        } else {
            return fn(err);
        }
    });
};


module.exports = Team;