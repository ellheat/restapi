/**
 * Created by patry on 22.01.2016.
 */

const Mysql = require('mysql');
const Config = require('../../config/config.js');

const database = Mysql.createConnection(Config.dbConnection);
const Player = {};
const Response = [
    {
        result: ""
    }
];


//url: /player

Player.getAllPlayers = function(fn) {
    database.query('SELECT * FROM player', function(err, rows) {
        if (!err) {
            return fn(rows);
        } else {
            return fn(err);
        }
    });
};

Player.addPlayer = function(payload, fn) {
    database.query('INSERT INTO player (idTeam, name, surname, jersey, age) VALUES("'+payload.idTeam+'","'+payload.name+'","'+payload.surname+'","'+payload.jersey+'","'+payload.age+'")', function(err) {
        if (!err) {
            Response[0].result = 'player has been added';
            return fn(Response);
        } else {
            return fn(err);
        }
    });
};


//url: /player/{id}

Player.getPlayer = function(id, fn) {
    database.query('SELECT * FROM player WHERE id='+id , function(err, rows) {
        if (rows != "") {
            return fn(rows);
        } else {
            return fn(err);
        }
    });
};

Player.updatePlayer = function(id, payload, fn) {
    database.query('UPDATE player SET idTeam="'+payload.idTeam+'", name="'+payload.name+'", surname="'+payload.surname+'", jersey="'+payload.jersey+'", age="'+payload.age+'" WHERE id='+id , function(err, res) {
        if (res.affectedRows > 0) {
            Response[0].result = 'player has been updated';
            return fn(Response);
        } else {
            return fn(err);
        }
    });
};

Player.deletePlayer = function(id, fn) {
    database.query('DELETE FROM player WHERE id='+id , function(err, res) {
        if (res.affectedRows > 0) {
            Response[0].result = 'player has been deleted';
            return fn(Response);
        } else {
            return fn(err);
        }
    });
};


module.exports = Player;