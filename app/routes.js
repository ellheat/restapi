/**
 * Created by patry on 22.01.2016.
 */

const Team = require('./models/team.js');
const Player = require('./models/player.js');
const Joi = require('Joi');
const Response = [
    {
        result: ""
    }
];

const routes = [
    {
        path: '/teams',
        method: 'GET',
        config: {
            handler: function(req, reply) {
                Team.getAllTeams(function (data) {
                    if (data) {
                        reply(data).code(200);
                    } else {
                        Response[0].result = "no content";
                        reply(Response).code(404);
                    }
                });
            },
            description: 'Get all teams',
            tags: ['api'],
            notes: ['Method to get all teams']
        }
    },
    {
        path: '/team',
        method: 'POST',
        config: {
            handler: function(req, reply) {
                Team.addTeam(req.payload, function (data, err) {
                    if (!err) {
                        reply(data).code(201);
                    } else {
                        reply(err).code(500);
                    }
                });
            },
            description: 'Add single team',
            tags: ['api'],
            notes: ['Method to add single team'],
            validate: {
                payload: {
                    name: Joi.string()
                            .required()
                            .description('The club name'),
                    city: Joi.string()
                            .required()
                            .description('The club city'),
                    state: Joi.string()
                            .required()
                            .description('The club state')
                }
            }
        }
    },
    {
        path: '/team/{id}',
        method: 'GET',
        config: {
            handler: function(req, reply) {
                Team.getSingleTeam(req.params.id, function (data) {
                    if (data) {
                        reply(data).code(200);
                    } else {
                        Response[0].result = "no content";
                        reply(Response).code(404);
                    }
                });
            },
            description: 'Get single teams',
            tags: ['api'],
            notes: ['Method to get single team'],
            validate: {
                params: {
                    id: Joi.number()
                        .required()
                        .description('Index club name')
                }
            }
        }
    },
    {
        path: '/team/{id}',
        method: 'PUT',
        config: {
            handler: function(req, reply) {
                Team.updateSingleTeam(req.params.id, req.payload, function (data) {
                    if (data) {
                        reply(data).code(204);
                    } else {
                        reply().code(500);
                    }
                });
            },
            description: 'Update single teams',
            tags: ['api'],
            notes: ['Method to update single team'],
            validate: {
                params: {
                    id: Joi.number()
                        .required()
                        .description('Index club name')
                },
                payload: {
                    name: Joi.string()
                        .required()
                        .description('The club name'),
                    city: Joi.string()
                        .required()
                        .description('The club city'),
                    state: Joi.string()
                        .required()
                        .description('The club state')
                }
            }
        }
    },
    {
        path: '/team/{id}',
        method: 'DELETE',
        config: {
            handler: function(req, reply) {
                Team.deleteSingleTeam(req.params.id, function (data) {
                    if (data) {
                        reply(data).code(200);
                    } else {
                        Response[0].result = "no content";
                        reply(Response).code(404);
                    }
                });
            },
            description: 'Delete single teams',
            tags: ['api'],
            notes: ['Method to delete single teams'],
            validate: {
                params: {
                    id: Joi.number()
                        .required()
                        .description('Index club name')
                }
            }
        }
    },
    {
        path: '/players',
        method: 'GET',
        config: {
            handler: function(req, reply) {
                Player.getAllPlayers(function (data) {
                    if (data) {
                        reply(data).code(200);
                    } else {
                        Response[0].result = "no content";
                        reply(Response).code(404);
                    }
                });
            },
            description: 'Get players from all teams',
            tags: ['api'],
            notes: ['Method to get all players']
        }
    },
    {
        path: '/player',
        method: 'POST',
        config: {
            handler: function(req, reply) {
                Player.addPlayer(req.payload, function (data, err) {
                    if (!err) {
                        reply(data).code(201);
                    } else {
                        reply(err).code(500);
                    }
                });
            },
            description: 'Add player',
            tags: ['api'],
            notes: ['Method to add single player'],
            validate: {
                payload: {
                    idTeam: Joi.number()
                        .required()
                        .description('Index club'),
                    name: Joi.string()
                        .required()
                        .description('The player name'),
                    surname: Joi.string()
                        .required()
                        .description('The player surname'),
                    jersey: Joi.number()
                        .required()
                        .description('The player number'),
                    age: Joi.number()
                        .required()
                        .description('The player age')
                }
            }
        }
    },
    {
        path: '/player/{id}',
        method: 'GET',
        config: {
            handler: function(req, reply) {
                Player.getPlayer(req.params.id, function (data) {
                    if (data) {
                        reply(data).code(200);
                    } else {
                        Response[0].result = "no content";
                        reply(Response).code(404);
                    }
                });
            },
            description: 'Get single player',
            tags: ['api'],
            notes: ['Method to get single player'],
            validate: {
                params: {
                    id: Joi.number()
                        .required()
                        .description('Index player')
                }
            }
        }
    },
    {
        path: '/player/{id}',
        method: 'PUT',
        config: {
            handler: function(req, reply) {
                Player.updatePlayer(req.params.id, req.payload, function (data) {
                    if (data) {
                        reply(data).code(204);
                    } else {
                        reply().code(500);
                    }
                });
            },
            description: 'Update player',
            tags: ['api'],
            notes: ['Method to update single player'],
            validate: {
                params: {
                    id: Joi.number()
                        .required()
                        .description('Index player')
                },
                payload: {
                    idTeam: Joi.number()
                        .required()
                        .description('Index club'),
                    name: Joi.string()
                        .required()
                        .description('The player name'),
                    surname: Joi.string()
                        .required()
                        .description('The player surname'),
                    jersey: Joi.number()
                        .required()
                        .description('The player number'),
                    age: Joi.number()
                        .required()
                        .description('The player age')
                }
            }
        }
    },
    {
        path: '/player/{id}',
        method: 'DELETE',
        config: {
            handler: function(req, reply) {
                Player.deletePlayer(req.params.id, function (data) {
                    if (data) {
                        reply(data).code(200);
                    } else {
                        Response[0].result = "no content";
                        reply(Response).code(404);
                    }
                });
            },
            description: 'Delete player',
            tags: ['api'],
            notes: ['Method to delete single player'],
            validate: {
                params: {
                    id: Joi.number()
                        .required()
                        .description('Index player')
                }
            }
        }
    }
];

module.exports = routes;