/**
 * Created by patry on 22.01.2016.
 */

const Config = require('./config/config.js');
const Routes = require('./app/routes.js');
const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Mysql = require('mysql');

const database = Mysql.createConnection(Config.dbConnection);
const server = new Hapi.Server();

database.connect();


const options = {
    info: {
        title: 'REST API Documentation',
        version: Pack.version
    }
};

server.connection(Config.serverConnection);

server.register([
    Inert,
    Vision,
    {
        register: HapiSwagger,
        options: options
    }], function(err) {
        server.start(function () {
            console.log('Server up and running at:', server.info.uri);
        });
    }
);

server.route(Routes);