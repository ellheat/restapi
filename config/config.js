/**
 * Created by patry on 22.01.2016.
 */

const config = {
    dbConnection: {
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: '',
        database: 'rest-api'
    },

    serverConnection: {
        host: 'localhost',
        port: 3000
    }
};

module.exports = config;